﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusLevelPosition : MonoBehaviour
{
    [SerializeField] int multiplayer = 1;
    readonly string ENEMY = "Enemy";

    GameController gameController;

    void Start()
    {
        gameController = GameController.GetController();
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag(ENEMY))
        {
            gameController.GetGate().SetMultiplayerValue(multiplayer);
        }
    }
}
