﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraController : MonoBehaviour
{

    [SerializeField] CinemachineVirtualCamera vCam;


    Vector3 zoomedIn = new Vector3(20f, 15f, -3f);
    Vector3 zoomedOut = new Vector3(30f, 20f, -3f);
    Vector3 aimOffset = new Vector3(0f, 0f, 3f);
    Vector3 aimOffset2 = new Vector3(0f, 3f, 3f);
    Vector3 bodyZoomInOffset = new Vector3(2f, 3f, -2f);
    Vector3 bodyZoomOutOffset = new Vector3(15f, 10f, -2f);

    WaitForSeconds WAIT = new WaitForSeconds(0.5f);

    CinemachineComponentBase baseBody;
    CinemachineComponentBase baseAim;

    void Start()
    {
        baseBody = vCam.GetCinemachineComponent(CinemachineCore.Stage.Body);
        baseAim = vCam.GetCinemachineComponent(CinemachineCore.Stage.Aim);
    }

    public void CameraZoomIn()
    {
        //CinemachineComponentBase componentBase = vCam.GetCinemachineComponent(CinemachineCore.Stage.Body);
        if (baseBody is CinemachineFramingTransposer)
        {
            (baseBody as CinemachineFramingTransposer).m_CameraDistance = 15;
            (baseBody as CinemachineFramingTransposer).m_TrackedObjectOffset = bodyZoomInOffset;
        }

        //CinemachineComponentBase componentBase2 = vCam.GetCinemachineComponent(CinemachineCore.Stage.Aim);
        if (baseAim is CinemachineComposer)
        {
            (baseAim as CinemachineComposer).m_TrackedObjectOffset = aimOffset2;
        }
    }
    public void ZoomOutCamera()
    {
        StartCoroutine(ZoomOutRoutine());
    }
    IEnumerator ZoomOutRoutine()
    {
        yield return WAIT;
        //CinemachineComponentBase componentBase = vCam.GetCinemachineComponent(CinemachineCore.Stage.Body);
        if (baseBody is CinemachineFramingTransposer)
        {
            (baseBody as CinemachineFramingTransposer).m_CameraDistance = 15;
            (baseBody as CinemachineFramingTransposer).m_TrackedObjectOffset = bodyZoomOutOffset;
        }

        //CinemachineComponentBase componentBase2 = vCam.GetCinemachineComponent(CinemachineCore.Stage.Aim);
        if (baseAim is CinemachineComposer)
        {
            (baseAim as CinemachineComposer).m_TrackedObjectOffset = aimOffset;
        }
    }
    public void ChangeTarget(Transform cameraTarget)
    {
        vCam.LookAt = cameraTarget;
        vCam.Follow = cameraTarget;
    }
}
