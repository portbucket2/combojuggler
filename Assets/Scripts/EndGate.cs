﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGate : MonoBehaviour
{
    [SerializeField] ParticleSystem confettiparticle;
    [SerializeField] Transform lastEnemy;
    [SerializeField] ParticleSystem hitParticle;

    [Header("debug field: ")]
    [SerializeField] bool isBonousMode = false;
    [SerializeField] int tapCounter = 0;
    [SerializeField] float timeCounter = 0;
    [SerializeField] float maxTime = 2;
    [SerializeField] int bonusMultiplayer = 1;
    [SerializeField] float flyingForce = 100f;
    [SerializeField] Rigidbody enemyRb ;

    readonly string PLAYER = "Player";
    Vector3 jumpDir = new Vector3(0f, 1f, 1f);
    WaitForSeconds WAIT = new WaitForSeconds(1.5f);
    bool enemyFlying = false;
    GameController gameController;

    void Start()
    {
        enemyRb = lastEnemy.GetComponent<Rigidbody>();
        gameController = GameController.GetController();
        jumpDir = jumpDir.normalized;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag(PLAYER))
        {
            GetComponent<BoxCollider>().enabled = false;
            other.GetComponent<PlayerController>().LevelEndReached();
            confettiparticle.Play();
            isBonousMode = true;
            gameController.GetPlayer().BonusLevelStarted(lastEnemy);
            gameController.GetUI().BonusLevelOn();
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && isBonousMode)
        {
            tapCounter++;
            hitParticle.Play();
        }

        if (isBonousMode)
        {
            timeCounter += Time.deltaTime;
            if (timeCounter>maxTime)
            {
                isBonousMode = false;
                gameController.GetUI().BonusLevelOff();
                ThrowEnemy();
                
            }
        }
    }
    void ThrowEnemy()
    {
        gameController.GetPlayer().LastPunch();
        enemyRb.isKinematic = false;
        enemyRb.AddForce(jumpDir * (5 + tapCounter) * flyingForce);
    }
    IEnumerator LevelEndRoutine()
    {
        yield return WAIT;
        gameController.LevelEnd();
    }
    public int GetTapCount()
    {
        return tapCounter;
    }
    public void SetMultiplayerValue(int value)
    {
        bonusMultiplayer = value;
        if (!enemyFlying)
        {
            enemyFlying = true;
            StartCoroutine(LevelEndRoutine());
        }
    }
}
