﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EnemyController : MonoBehaviour
{
    [SerializeField] float moveDistance = 2f;
    [SerializeField] GameObject enemyBody;
    [SerializeField] ParticleSystem hitParticle;

    [Header("Health:")]
    [SerializeField] int currentHealth = 5;
    [SerializeField] int maxHealth = 10;

    [Header("For Debug only:")]
    [SerializeField] Rigidbody rb;
    [SerializeField] bool isAlive = true;
    [SerializeField] List<Rigidbody> ragdollRbs;
    [SerializeField] Animator animator;

    Vector3 jumpDir = new Vector3(0f, 1f, 1f);

    readonly string DEFAULT = "Untagged";
    readonly string PLAYER = "Player";
    readonly string ENEMY = "Enemy";
    readonly string GROUND = "Ground";

    readonly string FALCON = "falcon";
    readonly string FLIP = "flip";
    readonly string ROUND = "round";
    readonly string SMASH = "smash";
    readonly string UPPERCUT = "uppercut";
    readonly string IDLE = "idle";
    readonly string PUNCH = "punch";
    readonly string LAND = "land";
    readonly string FALLING = "falling";
    readonly string DIEAIR = "dieAir";
    readonly string DIEGROUND = "dieGround";
    readonly string CELEBRATE = "celebrate";


    GameController gameController;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag(GROUND) && !isAlive)
        {
            animator.SetTrigger(DIEGROUND);
            //GetComponent<CapsuleCollider>().enabled = false;
        }
    }


    void Start()
    {
        gameController = GameController.GetController();
        rb = GetComponent<Rigidbody>();
        animator = enemyBody.GetComponent<Animator>();
        GetAllRigidBodiesInChildren(enemyBody.transform);
        RagDollKinematicTrue();
    }

    public void GetAttacked(string name)
    {
        if (isAlive)
        {
            
            animator.SetTrigger(name);
            hitParticle.Play();
            currentHealth--;
            rb.isKinematic = true;
            Vector3 pos = transform.position + jumpDir * moveDistance;
            transform.DOMove(pos, 0.5f).SetEase(Ease.Linear).OnComplete(AttackEnd);
            HealthCheck();
        }
    }
    public void AnimeDrop()
    {
        rb.AddForce(Vector3.down * 60f, ForceMode.VelocityChange);
    }
    public void StartFalling()
    {
        rb.isKinematic = false;
        //rb.velocity = Vector3.forward * 2f;
        transform.DOKill();
    }
    void AttackEnd()
    {
        //StartFalling();
    }

    void HealthCheck()
    {
        if (currentHealth <= 0)
        {
            isAlive = false;
            rb.isKinematic = true;
            //Debug.LogError("enemyDead!!!");
            //GetComponent<CapsuleCollider>().enabled = false;
            //animator.enabled = false;
            //RagDollKinematicFalse();
            transform.tag = DEFAULT;
            transform.DOKill();
            rb.isKinematic = false;
            gameController.CammeraZoomOut();
            gameController.GetPlayer().KilledEnemy();
            transform.DOMove(transform.position + Vector3.down * moveDistance, 0.25f).SetEase(Ease.Flash).OnComplete(DestroyObject);
            //rb.AddForce(Vector3.down * 1f, ForceMode.Impulse);
            
        }
    }
    void DestroyObject()
    {
        transform.DOKill();
        Destroy(gameObject, 5f);
    }

    void GetAllRigidBodiesInChildren(Transform trans)
    {
        foreach (Transform child in trans)
        {
            if (child.GetComponent<Rigidbody>())
            {
                ragdollRbs.Add(child.GetComponent<Rigidbody>());
            }
            if (child.childCount > 0)
            {
                GetAllRigidBodiesInChildren(child);
            }
            else
            {
                break;
            }
        }
    }
    void RagDollKinematicTrue()
    {
        int maxRag = ragdollRbs.Count;
        for (int i = 0; i < maxRag; i++)
        {
            ragdollRbs[i].isKinematic = true;
        }


    }
    void RagDollKinematicFalse()
    {
        int maxRag = ragdollRbs.Count;
        for (int i = 0; i < maxRag; i++)
        {
            ragdollRbs[i].isKinematic = false;
        }

    }



}
