﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    [SerializeField] UIController uiController;
    [SerializeField] PlayerController playerController;
    [SerializeField] EndGate endGate;
    [SerializeField] CameraController cameraController;


    public static GameController gameController;

    private void Awake()
    {
        gameController = this;
    }
    public static GameController GetController()
    {
        return gameController;
    }

    void Start()
    {
        
    }

    public PlayerController GetPlayer() { return playerController; }
    public UIController GetUI() { return uiController; }
    public EndGate GetGate() { return endGate; }
    public CameraController GetCamera() { return cameraController; }


    public void CammeraZoomOut()
    {
        cameraController.ZoomOutCamera();
    }

    public void LevelEnd()
    {
        Debug.Log("level end reached");
        uiController.LevelEnded();
    }
    public void FollowEnenyForBonous(Transform target)
    {

    }
}
