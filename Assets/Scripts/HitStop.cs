﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitStop : MonoBehaviour
{
    [SerializeField] float stopTime = 0.1f;
    [SerializeField] PlayerController playerController;

    bool waiting = false;

    WaitForSecondsRealtime WAIT;

    private void Awake()
    {
        WAIT = new WaitForSecondsRealtime(stopTime);
    }

    public void Stop()
    {
        //Debug.LogError("hitStop");
        if (waiting)
            return;

        playerController.HitCurrentEnemy();
        Time.timeScale = 0f;
        
        StopAllCoroutines();
        StartCoroutine(Waiting());
        
    }
    IEnumerator Waiting()
    {
        waiting = true;
        yield return WAIT;
        Time.timeScale = 1f;
        waiting = false;
    }
}
