﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingBuilding : MonoBehaviour
{
    [SerializeField] float spwanDistance = 15f;
    [SerializeField] float deSpwanDistance = 5f;

    GameController gameController;
    GameObject player;
    Rigidbody rb;
    //Vector3 currentVelocity = new Vector3(0f,0f,-1f);
    Vector3 heading;
    WorldScrolling ws;
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        ws = GetComponentInParent<WorldScrolling>();
    }
    void Start()
    {
        gameController = GameController.GetController();
        player = gameController.GetPlayer().gameObject;
        
    }
    private void Update()
    {
        heading = transform.position - player.transform.position;
        rb.velocity = ws.velocity; ;
        //Debug.LogError("Dot: " + Vector3.Dot(heading, player.transform.forward));
        if (Vector3.Distance(transform.position, player.transform.position) > deSpwanDistance && Vector3.Dot(heading, player.transform.forward) < 0)
        {
            // spwan front of the player
            MoveFrontOfTheLine();
        }
    }

    public void UpdateMovement( Vector3 _velocity)
    {
        if (gameController == null)
        {
            gameController = GameController.GetController();
            player = gameController.GetPlayer().gameObject;
        }

        //Debug.LogError("Dot: " + Vector3.Dot(transform.position, player.transform.position));
        rb.velocity = ws.velocity; ;

    }

    void MoveFrontOfTheLine()
    {
        //Debug.LogError("Dot: " + Vector3.Dot(transform.position, player.transform.position));
        rb.isKinematic = true;
        transform.position = new Vector3(transform.position.x, transform.position.y, player.transform.position.z + spwanDistance);
        rb.isKinematic = false;
        rb.velocity = ws.velocity;
    }
}
