﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Cinemachine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] bool isInputEnabled = true;
    [SerializeField] float moveDistance = 2f;
    [SerializeField] float moveSpeed = 2f;
    [SerializeField] float attackSpeed = 0.5f;
    [SerializeField] float findRadius = 4f;
    [SerializeField] Transform playerBody;
    [SerializeField] WorldScrolling scroller;
    [SerializeField] WorldScrolling scrollerBG;
    [SerializeField] ParticleSystem staticBuilding;

    [SerializeField] ParticleSystem speedTrail;
    [SerializeField] ParticleSystem landingSmoke;


    [Header("For Debug only:")]
    [SerializeField] bool isGrounded = false;
    [SerializeField] bool isFalling = false;
    [SerializeField] bool enemyFound = false;
    [SerializeField] Rigidbody rb;
    [SerializeField] EnemyController currentEnemy;
    [SerializeField] Animator animator;
    [SerializeField] string[] currentAttackName;
    [SerializeField] int currentAttack = 0;
    [SerializeField] float floatingTime = 1f;
    //[SerializeField] 

    readonly string PLAYER = "Player";
    readonly string ENEMY = "Enemy";
    readonly string GROUND = "Ground";


    readonly string FALCON = "falcon";
    readonly string FLIP = "flip";
    readonly string ROUND = "round";
    readonly string SMASH = "smash";
    readonly string UPPERCUT = "uppercut";
    readonly string IDLE = "idle";
    readonly string RUN = "run";
    readonly string LAND = "land";
    readonly string FALLING = "falling";
    readonly string DIEAIR = "dieAir";
    readonly string DIEGROUND = "dieGround";
    readonly string CELEBRATE = "celebrate";
    readonly string FAKE = "fake";
    readonly string LOOP = "loop";


    Vector3 jumpDir = new Vector3(0f, 1f, 1f);
    Vector3 HitPosOffset = new Vector3(0f, 1f, -1.5f);

    Vector3 zoomedIn = new Vector3(20f, 15f, -3f);
    Vector3 zoomedOut = new Vector3(30f, 20f, -3f);
    Vector3 aimOffset = new Vector3(0f, 0f, 3f);
    Vector3 aimOffset2 = new Vector3(0f, 3f, 3f);
    Vector3 bodyZoomInOffset = new Vector3(2f, 3f, -2f);
    Vector3 bodyZoomOutOffset = new Vector3(15f, 10f, -2f);

    GameController gameController;
    bool isBonusLevel = false;
    bool isFisrtMove = true;
    [SerializeField] float gravityCount = 0f;

    WaitForSeconds WAIT = new WaitForSeconds(0.5f);

    
    void Start()
    {
        gameController = GameController.GetController();
        jumpDir = jumpDir.normalized;
        rb = GetComponent<Rigidbody>();
        rb.velocity = Vector3.forward * moveSpeed;
        animator = transform.GetChild(0).GetComponent<Animator>();

        currentAttackName = new string[7];
        currentAttackName[0] = UPPERCUT;
        currentAttackName[1] = FALCON;
        currentAttackName[2] = ROUND;
        currentAttackName[3] = FLIP;
        currentAttackName[4] = ROUND;
        currentAttackName[5] = FLIP;
        currentAttackName[6] = SMASH;
        //speedTrail.Stop();
        //speedTrail.startSpeed = 5f;
    }

    // Update is called once per frame
    void Update()
    {
        if (gravityCount <= 0)
        {
            if (!isFalling)
            {
                isFalling = true;
                rb.isKinematic = false;
                animator.SetTrigger(FALLING);
                rb.AddForce(Vector3.down * 10f , ForceMode.Acceleration);
                if (currentEnemy)
                {
                    currentEnemy.StartFalling();
                }
            }
        }
        else
        {
            rb.isKinematic = true;
            gravityCount -= Time.deltaTime;
        }

        if (Input.GetMouseButtonDown(0))
        {

        }
        if (Input.GetMouseButton(0))
        {

        }
        if (Input.GetMouseButtonUp(0) && !isBonusLevel && isInputEnabled) 
        {
            AttackStart();
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag(GROUND))
        {
            GroundPlayer();
        }
    }

    void AttackStart()
    {
        
        rb.velocity = Vector3.zero;
        rb.isKinematic = true;
        isFalling = false;
        enemyFound = false;
        Transform enemy = null;
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, findRadius);
        foreach (var hitCollider in hitColliders)
        {
            if (hitCollider.CompareTag(ENEMY))
            {
                enemyFound = true;
                enemy = hitCollider.transform;
                break;
            }
        }

        if (enemyFound && enemy!= null)
        {
            if (currentAttack < currentAttackName.Length)
            {
                speedTrail.Play();

                if (!isFisrtMove)
                {
                    isFisrtMove = false;
                    scroller.UpdateScrollSpeed(500f);
                    scrollerBG.UpdateScrollSpeed(500f);
                }
                //Debug.LogError("found enemy");
                isGrounded = false;
                currentEnemy = enemy.GetComponent<EnemyController>();
                GoAndAttack(enemy.transform.position);
                animator.SetTrigger(currentAttackName[currentAttack]);
                currentAttack++;
                DisableInput();

                //SpeedUpParticles(200f, speedTrail);
                speedTrail.Play();
            }
        }
        else
        {
            JumpUp();
        }
    }
    void AttackEnd()
    {
        

        EnableInput();
        gravityCount = floatingTime;
        
    }
    void JumpUp()
    {
        gravityCount = floatingTime;
        animator.SetTrigger(FAKE);
        Vector3 pos = transform.position + Vector3.up * moveDistance;
        transform.DOMove(pos, 0.2f).SetEase(Ease.Flash).OnComplete(AttackEnd);

        scroller.UpdateScrollSpeed(0f);
        scrollerBG.UpdateScrollSpeed(0f);
        speedTrail.Stop();

    }
    void GoAndAttack(Vector3 enemyPos)
    {
        gravityCount = floatingTime;
        transform.DOKill();
        Vector3 pos = enemyPos + HitPosOffset;
        transform.DOMove(pos, attackSpeed).SetEase(Ease.Flash).OnComplete(GoAndHitEnemy) ;
        CameraZoomIn();
    }
    void GoAndHitEnemy()
    {
        //currentEnemy.GetAttacked();
        if (isFisrtMove)
        {
            isFisrtMove = false;
            scroller.UpdateScrollSpeed(500f);
            scrollerBG.UpdateScrollSpeed(500f);
        }

        AttackEnd();
    }
    void CameraZoomIn()
    {
        gameController.GetCamera().CameraZoomIn();
    }


    public void SpeedUpParticles(float acceleration, ParticleSystem party)
    {
        ParticleSystem particleSystem = party;
        ParticleSystem.Particle[] particles;


        particles = new ParticleSystem.Particle[particleSystem.main.maxParticles];

        int aliveParticlesCount = particleSystem.GetParticles(particles);

        // Change only the particles that are alive
        for (int i = 0; i < aliveParticlesCount; i++)
        {
            particles[i].velocity = particles[i].velocity.normalized * (particles[i].velocity.magnitude + Time.deltaTime);
        }
        particleSystem.SetParticles(particles, aliveParticlesCount);
    }




    public void EnableInput() { isInputEnabled = true; }
    public void DisableInput() { isInputEnabled = false; }

    public void HitCurrentEnemy()
    {
        if (currentEnemy)
        {
            currentEnemy.GetAttacked(currentAttackName[currentAttack - 1]);

            //speedTrail.startSpeed = 5f;
            //scrollBuilding1.startSpeed = 50f;
            //scrollBuilding2.startSpeed = 50f;

            scroller.UpdateScrollSpeed(50f);
            scrollerBG.UpdateScrollSpeed(50f);

            //SpeedUpParticles(15f, speedTrail);

            //Debug.LogError("Hit Current enemy!");
        }
    }

    public void LevelEndReached()
    {
        rb.velocity = Vector3.zero;
        //rb.isKinematic = true;
        
        //gameController.LevelEnd();
    }
    public void BonusLevelStarted(Transform cameraTarget)
    {
        isBonusLevel = true;
        rb.velocity = Vector3.zero;
        animator.SetTrigger(LOOP);
        gameController.GetCamera().ChangeTarget(cameraTarget);
    }
    public void LastPunch()
    {
        animator.SetTrigger(FAKE);
    }
    public void KilledEnemy()
    {
        Debug.LogError("Enemy Killed");
        isFisrtMove = true;
        gravityCount = 0.4f;

        StartCoroutine(KilledEnemyRoutine());
    }
    IEnumerator KilledEnemyRoutine()
    {
        yield return WAIT;
        rb.AddForce(Vector3.down * 60f, ForceMode.VelocityChange);
        currentEnemy.AnimeDrop();

        scroller.UpdateScrollSpeed(0f);
        scrollerBG.UpdateScrollSpeed(0f);
        staticBuilding.transform.position = new Vector3(0f, -22.9f, transform.position.z);
        staticBuilding.Play();
        speedTrail.Stop();
    }

    public void GroundPlayer()
    {
        landingSmoke.Play();
        isFisrtMove = true;
        scroller.UpdateScrollSpeed(0f);
        scrollerBG.UpdateScrollSpeed(0f);
        //rb.isKinematic = true;
        gravityCount = 0;
        isGrounded = true;
        animator.SetTrigger(LAND);
        currentAttack = 0;
        //rb.isKinematic = false;
        rb.velocity = Vector3.forward * moveSpeed;
    }
}
