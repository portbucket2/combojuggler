﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    [SerializeField] GameObject resetPanel;
    [SerializeField] Button resetButton;
    [SerializeField] GameObject bonusLevelPanel;

    void Start()
    {
        resetButton.onClick.AddListener(delegate {
            SceneManager.LoadScene(0);
	    });
    }
    public void LevelEnded()
    {
        resetPanel.SetActive(true);
    }
    public void BonusLevelOn() { bonusLevelPanel.SetActive(true); }
    public void BonusLevelOff() { bonusLevelPanel.SetActive(false); }
}
