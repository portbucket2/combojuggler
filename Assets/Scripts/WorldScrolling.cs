﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WorldScrolling : MonoBehaviour
{
    //[SerializeField] public float speed = 2f;
    [SerializeField] int buildingsCount = 0;
    [SerializeField] public Vector3 velocity = new Vector3(0f,0f,-1f);
    [SerializeField] List<MovingBuilding> buildings;

    GameController gameController;
    Transform player;

    void Start()
    {
        gameController = GameController.GetController();
        player = gameController.GetPlayer().transform;

        //velocity = new Vector3(0f, 0f, -1f * speed);
        buildings = new List<MovingBuilding>();
        buildingsCount = transform.childCount;
        for (int i = 0; i < buildingsCount; i++)
        {
            buildings.Add(transform.GetChild(i).GetComponent<MovingBuilding>());
        }

        for (int i = 0; i < buildingsCount; i++)
        {
            buildings[i].UpdateMovement(velocity);
        }
    }

    public void UpdateScrollSpeed(float _speed) {
        velocity = new Vector3(0f, 0f, -1f * _speed);
        //for (int i = 0; i < buildingsCount; i++)
        //{
        //    buildings[i].UpdateMovement(velocity);
        //}
    }

}
